<?php
/**
 * Part of the Caffeinated PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\IdeaMeta\Metas;

use Sebwite\Support\Path;
use Sebwite\Support\Str;

/**
 * This is the BitbucketMeta.
 *
 * @package        Sebwite\Workbench
 * @author         Caffeinated Dev Team
 * @copyright      Copyright (c) 2015, Caffeinated
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class BitbucketMeta extends BaseMeta
{
    protected $template = <<<'EOF'
@foreach($methods as $method)
    {!! $method !!} => [
        '' == '@',
        @foreach($data as $k => $v)
            '{!! $k !!}' instanceof {!! \Sebwite\Support\Str::ensureLeft($v, '\\') !!},
        @endforeach
    ],
@endforeach
EOF;

    protected $methods = [
        'new \Bitbucket\API\Api',
        '\Bitbucket\API\Api::api(\'\')',
    ];


    public function getData()
    {
        $classes = [];
        $apiClasses = [ 'GroupPrivileges', 'Groups',  'Invitations', 'Privileges', 'Repositories', 'Teams', 'User', 'Users' ];
        $apiDir = base_path(Path::join('vendor', 'gentle', 'bitbucket-api', 'lib', 'Bitbucket', 'API'));
        foreach (['Repositories', 'User', 'Users', 'Groups'] as $dir) {
            $files = array_merge(
                glob(Path::join($apiDir, $dir, '*.php')),
                glob(Path::join($apiDir, $dir, '*/*.php'))
            );
            foreach ($files as $filePath) {
                $ext = Path::getExtension($filePath);
                $rel = Path::makeRelative($filePath, $apiDir);
                $res = Str::removeRight($rel, '.' . $ext);
                $apiClasses[] = Str::replace($res, '/', '\\');
            }
        }



        #['BranchRestrictions', 'Changesets', 'Commits', 'Deploykeys']
        foreach ($apiClasses as $apiClass) {
            $classes[$apiClass] = 'Bitbucket\\API\\' . $apiClass;
        }
        return $classes;
    }

    public static function canRun()
    {
        return class_exists('Bitbucket\API\Api', false);
    }
}
