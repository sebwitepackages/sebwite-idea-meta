<?php
namespace Sebwite\IdeaMeta;

class Util
{

    /**
     * Joins a split file system path.
     *
     * @param  array|string $path
     *
     * @return string
     */
    public static function join()
    {
        $arguments = func_get_args();

        if (func_num_args() === 1 and is_array($arguments[ 0 ])) {
            $arguments = $arguments[ 0 ];
        }

        foreach ($arguments as $key => &$argument) {
            if (is_array($argument)) {
                $argument = static::join($argument);
            }
            $argument = static::removeRight($argument, '/');

            if ($key > 0) {
                $argument = static::removeLeft($argument, '/');
            }

            #$arguments[ $key ] = $argument;
        }

        return implode(DIRECTORY_SEPARATOR, $arguments);
    }


    /**
     * Returns a new string with the prefix $substring removed, if present.
     *
     * @param  string  $substring The prefix to remove
     * @return Stringy Object having a $str without the prefix $substring
     */
    public static function removeLeft($str, $substring)
    {
        if (static::startsWith($str, $substring)) {
            $substringLength = mb_strlen($substring);
            return substr($str, $substringLength);
        }

        return $str;
    }

    /**
     * Returns a new string with the suffix $substring removed, if present.
     *
     * @param  string  $substring The suffix to remove
     * @return Stringy Object having a $str without the suffix $substring
     */
    public static function removeRight($str, $substring)
    {
        if (static::endsWith($str, $substring)) {
            return substr($str, 0, strlen($str) - strlen($substring));
        }

        return $str;
    }


    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     *
     * @author Taylor Otwell
     */
    public static function startsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle !== '' && strpos($haystack, $needle) === 0) {
                return true;
            }
        }

        return false;
    }


    /**
     * Determine if a given string ends with a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     *
     * @author Taylor Otwell
     */
    public static function endsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ((string) $needle === substr($haystack, -strlen($needle))) {
                return true;
            }
        }

        return false;
    }

    public static function getClassNameFromFile($filePath)
    {
        $class = '';
        try {
            $fp = fopen($filePath, 'r');


            $class = $buffer = '';
            $i     = 0;
            while (!$class) {
                if (feof($fp)) {
                    break;
                }

                $buffer .= fread($fp, 512);
                $tokens = token_get_all($buffer);

                if (strpos($buffer, '{') === false) {
                    continue;
                }

                for (; $i < count($tokens); $i++) {
                    if ($tokens[ $i ][ 0 ] === T_CLASS) {
                        for ($j = $i + 1; $j < count($tokens); $j++) {
                            if ($tokens[ $j ] === '{') {
                                $class = $tokens[ $i + 2 ][ 1 ];
                            }
                        }
                    }
                }
            }
        } catch (\Throwable $e) {

        }

        return $class;
    }
}
