<?php

namespace Sebwite\IdeaMeta;

use Illuminate\Support\ServiceProvider;

/**
 * The main service provider
 *
 * @author        Sebwite
 * @copyright     Copyright (c) 2015, Sebwite
 * @license       http://mit-license.org MIT
 * @package       Sebwite\IdeaMeta
 */
class IdeaMetaServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $viewPath = __DIR__ . '/../resources/views';
        $this->loadViewsFrom($viewPath, 'idea-meta');

        $configPath = __DIR__ . '/../config/idea-meta.php';
        if (function_exists('config_path')) {
            $publishPath = config_path('idea-meta.php');
        } else {
            $publishPath = base_path('config/idea-meta.php');
        }
        $this->publishes([ $configPath => $publishPath ], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/idea-meta.php';
        $this->mergeConfigFrom($configPath, 'idea-meta');

        $this->app->singleton('idea-meta', MetaRepository::class);
        $this->app->alias('idea-meta', MetaRepositoryInterface::class);

        $this->app[ 'command.idea-meta.generate' ] = $this->app->share(function ($app) {
            return $app->make(Commands\MetaCommand::class);
        });

        $this->commands('command.idea-meta.generate');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [ 'command.idea-meta.generate', 'idea-meta' ];
    }
}
