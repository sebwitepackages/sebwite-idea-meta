<?php

return [

    'output' => '.phpstorm.meta.php',
    'view'   => 'idea-meta::meta',
    'metas'  => [
        'bindings'       => 'Sebwite\IdeaMeta\Metas\BindingsMeta',
        'config'         => 'Sebwite\IdeaMeta\Metas\ConfigMeta',
        'routes'         => 'Sebwite\IdeaMeta\Metas\RoutesMeta',
       # 'translations' => 'Sebwite\IdeaMeta\Metas\TransMeta',

        // will skip these if classes cannot be found
        #'bitbucket'      => 'Sebwite\IdeaMeta\Metas\BitbucketMeta',
        'codex'          => 'Sebwite\IdeaMeta\Metas\CodexMeta',
        #'codex-projects' => 'Sebwite\IdeaMeta\Metas\CodexProjectsMeta',
    ],
];
