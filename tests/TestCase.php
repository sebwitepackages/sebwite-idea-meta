<?php

namespace Sebwite\Tests\IdeaMeta;

abstract class TestCase extends \Sebwite\Testbench\TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getServiceProviderClass()
    {
        return \Sebwite\IdeaMeta\IdeaMetaServiceProvider::class;
    }

   /**
    * {@inheritdoc}
    */
    protected function getPackageRootPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..';
    }
}
